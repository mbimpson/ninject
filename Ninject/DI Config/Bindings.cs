﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

namespace NinjectExample
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IHelper>().To<HelperClass>();
            Bind<IHelper2>().To<Helper2>();
        }
    }
}
