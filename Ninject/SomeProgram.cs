﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NinjectExample
{
    public class SomeProgram
    {
        private readonly IHelper _helper;
        public SomeProgram(IHelper helper)
        {
            this._helper = helper;

            Console.WriteLine(this._helper.Get());
            Console.ReadKey();
        }
    }
}
