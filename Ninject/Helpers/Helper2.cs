﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NinjectExample
{
    public class Helper2 : IHelper2
    {
        public string Get()
        {
            return "Nested dependency working!";
        }
    }
}
