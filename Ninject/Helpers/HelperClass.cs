﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NinjectExample
{
    public class HelperClass : IHelper
    {
        private readonly IHelper2 helper2;

        public HelperClass(IHelper2 helper2)
        {
            this.helper2 = helper2;
        }
        
        public string Get()
        {
            var helper2Result = this.helper2.Get();
            return "First dependency working!" + Environment.NewLine + helper2Result;
        }
    }
}
