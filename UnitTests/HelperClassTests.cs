﻿using System;
using NinjectExample;
using NUnit.Framework;
using Moq;

namespace UnitTests
{
    [TestFixture]
    public class HelperClassTests
    {
        private Mock<IHelper2> mockHelper2;
        private HelperClass helperClass;

        [SetUp]
        public void Setup()
        {
            mockHelper2 = new Mock<IHelper2>();
            mockHelper2.Setup(x => x.Get()).Returns("Response from helper2");

            helperClass = new HelperClass(mockHelper2.Object);
        }

        [Test]
        public void GetReturnsCorrectValue()
        {
            string result = helperClass.Get();
            string expected = "First dependency working!" + Environment.NewLine + "Response from helper2";

            Assert.AreEqual(result, expected);
        }
    }
}
