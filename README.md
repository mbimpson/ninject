# Ninject#

### What is this repository for? ###

* Quick demonstration of dependency injection using Ninject
* Also demonstrates how to mock injected dependencies (using Moq)

### How do I get set up? ###
* Open Ninject.sln with Visual Studio in ninject\Ninject

### Overview ###

* The dependency bindings are made in Bindings.cs. Create a class which inherits from NinjectModule (Ninject.Modules) and specify your bindings in the Load() method. This method will be run when the application starts.

* To unit test an injected dependency (in this case a constructor injection dependency), mock up the dependency and then pass in the mock.Object into your classes constructor.